import React from 'react';
import { StyleSheet, View } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { styles } from './styles/Styles';

export default function MapScreen() {

  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: -20.398259,
          longitude: -43.507726,
          latitudeDelta: 0.0522,
          longitudeDelta: 0.0121,
        }}
      >
        {/* Seus marcadores vão aqui */}
      </MapView>

      <GooglePlacesAutocomplete
        placeholder="Pesquisar"
        onPress={(data, details = null) => {
          // 'details' contém informações sobre o local selecionado
          console.log(data, details);
        }}
        query={{
          key: 'SuaChaveDeAPIAqui',
          language: 'pt', // ou o idioma desejado
        }}
        styles={{
          textInputContainer: {
            width: '100%',
          },
          description: {
            fontWeight: 'bold',
          },
        }}
      />
    </View>
  );
}